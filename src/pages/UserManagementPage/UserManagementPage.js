import { message, Table } from "antd";
import HeaderTheme from "components/Header/Header";
import React, { useEffect, useState } from "react";
import { userServ } from "service/user.service";
import { createHeaderUserTable, headerUserTable } from "./userUtils";

export default function UserManagementPage() {
  const [userList, setUserList] = useState([]);

  console.table(userList);
  let fetchUserList = () => {
    userServ
      .getUserList()
      .then((res) => {
        let dataRaw = res.data.content;
        setUserList(dataRaw);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let handleDeleteUser = (taiKhoan) => {
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        // refresh lại table sau khi xoá thành công
        message.success("Xoá thành công");
        fetchUserList();
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
  useEffect(() => {
    fetchUserList();
  }, []);
  return (
    <div className="container mx-auto">
      <HeaderTheme />
      <Table
        dataSource={userList}
        columns={createHeaderUserTable(handleDeleteUser)}
      />
    </div>
  );
}
