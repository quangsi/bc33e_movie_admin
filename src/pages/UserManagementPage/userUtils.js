import { Button, Tag } from "antd";

export const createHeaderUserTable = (onDelete) => {
  return [
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Họ tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Số điện thoại",
      dataIndex: "soDT",
      key: "soDT",
    },
    {
      title: "Loại người dùng",
      dataIndex: "maLoaiNguoiDung",
      key: "maLoaiNguoiDung",
      render: (text, user) => {
        if (text == "QuanTri") {
          return <Tag color="green">Quản trị </Tag>;
        } else {
          return <Tag color="blue">Khách hàng</Tag>;
        }
      },
    },
    {
      title: "Thao tác",
      key: "action",
      render: (_, user) => {
        return (
          <div className="space-x-px">
            <Button className=" bg-blue-500 text-white">Sửa</Button>
            <Button
              onClick={() => {
                onDelete(user.taiKhoan);
              }}
              className=" bg-red-500 text-white"
            >
              Xoá
            </Button>
          </div>
        );
      },
    },
  ];
};

// taiKhoan
// hoTen
// email
// soDT
// matKhau
// maLoaiNguoiDun
