import axios from "axios";
import { BASE_URL, configHeaders, https } from "./url.config";

export const userServ = {
  postLogin: (dataLogin) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
    //   method: "POST",
    //   data: dataLogin,
    //   headers: configHeaders(),
    // });
    let uri = "/api/QuanLyNguoiDung/DangNhap";
    return https.post(uri, dataLogin);
  },

  getUserList: () => {
    let uri = "/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP10";
    return https.get(uri);
  },
  deleteUser: (taiKhoan) => {
    let uri = `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`;

    return https.delete(uri);
  },
};

// account quantri
//tk: admin005 - pass: admin0031
