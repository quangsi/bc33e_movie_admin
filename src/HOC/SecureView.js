import React from "react";
import { useEffect } from "react";
import { userInfoLocal } from "service/local.service";

export default function SecureView({ Component }) {
  //
  useEffect(() => {
    let user = userInfoLocal.get();
    console.log("user: ", user);
    if (user == null || user.maLoaiNguoiDung !== "QuanTri") {
      window.location.href = "/login";
      userInfoLocal.remove();
    }
  }, []);
  return <Component />;
}
// high order function

// high order component

// responsive

// sass
